package com.devopssample.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
* This is a public class.
*  
*/
@Controller
@RequestMapping(value = "/")
public class FrontController{
	
	/**
	* This is a public method.
	* 
	* @param request The request parameter.
	*
	* @return it will return the model.
	*/
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView getHomeView(HttpServletRequest request) {
		StringBuilder str=new StringBuilder("test");
		str.append("string");
		String str1="An issue that represents something wrong in the code. If this has not broken yet, it will, and probably at the worst possible"+ "moment. This needs to be fixed. Yesterday.";	

		if(str1.contains("z")) {
			System.err.println(str1);
		}
		else {
			System.err.println(str);
		}
		return new ModelAndView("home");
	}
}
